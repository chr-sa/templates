{
  inputs = {nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";};

  outputs = {nixpkgs, ...}: let
    forAllSystems = function:
      nixpkgs.lib.genAttrs ["x86_64-linux" "aarch64-darwin"]
      (system: function nixpkgs.legacyPackages.${system});
    pythonPackages = p:
      with p; [
        numpy
        scipy
        matplotlib
        polars
      ];
  in {
    devShells = forAllSystems (pkgs: {
      default = pkgs.mkShell {
        packages = [(pkgs.python3.withPackages pythonPackages)];
      };
    });
  };
}
